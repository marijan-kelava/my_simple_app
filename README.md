# My_simple_app

## Installation

Checkout `master` branch

## Docker Setup

Run `docker-compose up -d` to build up the containers

After that, login to `my_simple_app_web` container `docker exec -it my_simple_app_web bash` (var/www), and run  `composer install` 

Go to: `http://localhost:8888`  

Login to adminer use container name as server, 


