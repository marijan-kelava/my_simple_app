<?php

namespace App;

use App\Router;
use App\Db\Database;

$connection = Database::getInstance();
$pdo = $connection->getPdo();
$connection->createTables();

$uri = $_SERVER['REQUEST_URI'];

//Include the routes file directly
require 'routes.php';

// Now you can use the $router variable directly
$router->dispatch($uri);













