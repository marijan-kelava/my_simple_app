<?php

//Database configuration
$configuration = array(
    'db_dsn'  => 'mysql:host=my_simple_app_db;port=3306;dbname=db',
    'db_user' => 'user',
    'db_pass' => 'user'
);

return $configuration;