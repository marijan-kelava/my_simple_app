<?php

use App\Router;
use App\Controllers\LoginController;
use App\Controllers\HomeController;
use App\Controllers\UserController;
use App\Controllers\RegisterController;
use App\Controllers\ManagementController;
use App\Controllers\MyAccountController;
use App\Controllers\AssetController;

$router = new Router();

$router->addRoute('/', HomeController::class, 'index');
$router->addRoute('/assets/count', AssetController::class, 'imagesCount');
$router->addRoute('/login', LoginController::class, 'index');
$router->addRoute('/auth/login', LoginController::class, 'auth');
$router->addRoute('/logout', LoginController::class, 'logout');
$router->addRoute('/register', RegisterController::class, 'index');
$router->addRoute('/user/register', RegisterController::class, 'user');
$router->addRoute('/management', ManagementController::class, 'index');
$router->addRoute('/asset/management', ManagementController::class, 'asset');
$router->addRoute('/myaccount', MyAccountController::class, 'index');
$router->addRoute('/user/myaccount', MyAccountController::class, 'user');
    