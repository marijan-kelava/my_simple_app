<?php

namespace App\Services;

use \PDO;

class AssetsService
{
    public function __construct(
        private \PDO $connection
    ) {}

    public function getImagesCount()
    {
        $sql = 'SELECT COUNT(*) as imagesCount FROM assets';
        $statement = $this->connection->query($sql);

        $statement->execute();

        $result = $statement->fetch(PDO::FETCH_ASSOC);
        
        return $result;
    }

    public function getAssets($id): ?array
    {
        $query = 'SELECT assets.id AS asset_id, assets.user_id AS asset_user_id, assets.imagePath, ';
        $query .= 'users.id, users.userName, users.email ';
        $query .= 'FROM assets LEFT JOIN users ON assets.user_id = users.id ';

        $stmt = $this->connection->prepare($query);
        
        $stmt->execute();
        
        $users = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $users;  
    }
    
    //Find and return user id according to user email
    public function prepareImageUpload($users) : ?int
    {   
        $id = null;
        if (isset($_SESSION['email'])) {
            $email = $_SESSION['email'];
            
            function findUserAndGetUserId($email, $users) {
                $foundUsers = array_filter($users, function ($user) use ($email) {
                    return $user['email'] === $email;
                });
                    
                    return $foundUsers;
            }

            $result = findUserAndGetUserId($email, $users);
        
            foreach ($result as $value) {
                $id = $value['id'];
            }
            
            return $id;
        }
            return null;
    }

    public function uploadImage($id)
    {        
        if (empty($_FILES)) {
            exit;
        }
        
        if ($_FILES["image"]["error"] !== UPLOAD_ERR_OK) {
        
            switch ($_FILES["image"]["error"]) {
                case UPLOAD_ERR_PARTIAL:
                    exit('File only partially uploaded');
                    break;
                case UPLOAD_ERR_NO_FILE:
                    exit('No file was uploaded');
                    break;
                case UPLOAD_ERR_EXTENSION:
                    exit('File upload stopped by a PHP extension');
                    break;
                case UPLOAD_ERR_FORM_SIZE:
                    exit('File exceeds MAX_FILE_SIZE in the HTML form');
                    break;
                case UPLOAD_ERR_INI_SIZE:
                    exit('File exceeds upload_max_filesize in php.ini');
                    break;
                case UPLOAD_ERR_NO_TMP_DIR:
                    exit('Temporary folder not found');
                    break;
                case UPLOAD_ERR_CANT_WRITE:
                    exit('Failed to write file');
                    break;
                default:
                    exit('Unknown upload error');
                    break;
            }
        }
        
        if ($_FILES["image"]["size"] > 1048576) {
            exit('File too large (max 1MB)');
        }
        
        
        $finfo = new \finfo(FILEINFO_MIME_TYPE);
        $mime_type = $finfo->file($_FILES["image"]["tmp_name"]);
        $allowed_mime_types = ["image/png", "image/jpg", "image/jpeg"];

        if (!in_array($mime_type, $allowed_mime_types)) {
        exit("Invalid file type");
        }
        
        $pathinfo = pathinfo($_FILES["image"]["name"]);
        
        $base = $pathinfo["filename"];
        
        $base = preg_replace("/[^\w-]/", "_", $base);
        
        $filename = $base . "." . $pathinfo["extension"];
        
        $destination =  __DIR__ . "/../../public/uploads/" . $filename;
        
        if ( ! move_uploaded_file($_FILES["image"]["tmp_name"], $destination)) {
        
            exit("Can't move uploaded file");
        
        }
        
        $imagePath = "uploads/" . $filename ;
        
        $user_id = $id;
        
        $sql = "INSERT INTO assets (user_id, imagePath)
                VALUES (?, ?)";

        $stmt = $this->connection->prepare($sql);
                
        $stmt->bindParam(1, $user_id, PDO::PARAM_STR);
        $stmt->bindParam(2, $imagePath, PDO::PARAM_STR);

        $stmt->execute();   
    }

    public function removeImage($asset_id)
    {
        $sql = "DELETE FROM assets WHERE id = :id";
        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam(':id', $asset_id, PDO::PARAM_STR);

        $stmt->execute();     
    }
}   