<?php

namespace App\Services;

use \PDO;

final class UserService
{
    public function __construct(
        private \PDO $connection
    ) {}

    public function countRegisteredUsers(): array
    {
        $sql = 'SELECT COUNT(*) FROM users';
        $statement = $this->connection->query($sql);

        $statement->execute();

        return $result = $statement->fetch(PDO::FETCH_ASSOC);
    }
    
    public function getUsers() : ?array
    {
        $query = 'SELECT users.id, users.userName, users.email, ';
        $query .= 'assets.id AS asset_id, assets.user_id AS asset_user_id, assets.imagePath ';
        $query .= 'FROM users LEFT JOIN assets ON users.id = assets.user_id';

        $stmt = $this->connection->prepare($query);
        
        $stmt->execute();

        $users = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $users;  
    }

    public function getRegisteredUsers(): ?array
    {
        $query = 'SELECT users.id, users.userName, users.email FROM users';

        $stmt = $this->connection->prepare($query);
        
        $stmt->execute();

        $users = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $users;  
        
        
    }

    public function getUserById(int $id)
    {
        $query = "SELECT * FROM users WHERE id = :id";

        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
        $stmt->execute();

        $user = $stmt->fetch(\PDO::FETCH_NUM);

        return $user;     
    }

    public function getUserByEmail(?string $email): array
    {   
        $query = "SELECT * FROM users WHERE email = :email";

        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(':email', $email, \PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(\PDO::FETCH_ASSOC);

        if (!empty($user)) {
            return $user;
        }else {
            return [];
        }
    }

    public function login($email, $password, $registeredUser)
    {   
        if (password_verify($password, $registeredUser['password']) && $email === $registeredUser['email']) {
            $_SESSION['email'] = $email;
            $token = $registeredUser['token'];
            setcookie("rememberMe", $token, time() + (60 * 60), "/");
        }
    }

    public function getUserByRememberMeToken($token): array
    {
        $query = "SELECT * FROM users WHERE token = :token";

        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(':token', $token, \PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(\PDO::FETCH_ASSOC);

        if (empty($user)) {
            return [];
        }else {
            return $user;
        }
    }

    public function saveNewUser(array $data) 
    {
        $password_hash = password_hash($data["password"], PASSWORD_DEFAULT);

        $hashedToken = $this->generateSecureToken();
        
        $sql = "INSERT INTO users (userName, email, password, token)
                VALUES (?, ?, ?, ?)";
                
        $stmt = $this->connection->prepare($sql);

        $stmt->bindParam(1, $data['name'], PDO::PARAM_STR);
        $stmt->bindParam(2, $data['email'], PDO::PARAM_STR);
        $stmt->bindParam(3, $password_hash, PDO::PARAM_STR);
        $stmt->bindParam(4, $hashedToken, PDO::PARAM_STR);

        $stmt->execute();
    }

    public function updateUser($newPassword, $email)
    {
        if (empty($_POST)) {
            die;
        }

        if (strlen($_POST["newPassword"]) < 8) {
            die("Password must be at least 8 characters");
        }
        
        if ( ! preg_match("/[a-z]/i", $_POST["newPassword"])) {
            die("Password must contain at least one letter");
        }
        
        if ( ! preg_match("/[0-9]/", $_POST["newPassword"])) {
            die("Password must contain at least one number");
        }
        
        if ($_POST["newPassword"] !== $_POST["password_confirmation"]) {
            die("Passwords must match");
        }
        
        $password_hash = password_hash($_POST["newPassword"], PASSWORD_DEFAULT);

        $sql = "UPDATE users SET password = :newPassword WHERE email = :email";
                
        $stmt = $this->connection->prepare($sql);

        $stmt->bindParam(':newPassword', $password_hash, PDO::PARAM_STR);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);

        $stmt->execute();
    }

    public function deleteUser($email)
    {
        $sql = "DELETE FROM users WHERE email = :email";
        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);

        $stmt->execute();   
    }

    public function validateRegistrationRequest(array $data): array
    {
        $errors = [];

        if (empty($data)) {
            $errors['registerRequest'] = 'All fields must be set';
        }

        if (empty($data["name"])) {
            $errors['name'] = 'Name is required';
        }
        
        if ( ! filter_var($data["email"], FILTER_VALIDATE_EMAIL)) {
            $errors['email'] = "Valid email is required";
        }
        
        if (strlen($data["password"]) < 8) {
            $errors['password'] = 'Password must be at least 8 characters';
        }
        
        if ( ! preg_match("/[a-z]/i", $data["password"])) {
            $errors['password'] = 'Password must contain at least one letter';
        }
        
        if ( ! preg_match("/[0-9]/", $data["password"])) {
            $errors['password'] = 'Password must contain at least one number';
        }
        
        if ($data["password"] !== $data["password_confirmation"]) {
            $errors['password'] = 'Passwords must match';
        }

        if(count($errors) > 0){
            return $errors;
        }

        return [];
    }

    private function generateSecureToken():string 
    {
        //Generate a random string
        $randomString = bin2hex(random_bytes(32));
    
        //Hash the random string using SHA-256
        $hashedToken = hash('sha256', $randomString);
    
        return $hashedToken;
    }
}