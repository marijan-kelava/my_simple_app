<?php

namespace App\Db;

final class Database
{
    private static $instance = null;
    private \PDO $pdo;
    
    private array $configuration = [];
    
    private function __construct()
    {
        $this->configuration = require(__DIR__ . '/../db_config.php');
        
        try {
            $this->pdo = new \PDO($this->configuration['db_dsn'], $this->configuration['db_user'], $this->configuration['db_pass'], [
                \PDO::ATTR_PERSISTENT => 'true',
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
            ]);
        } catch (\Exception $e) {
            throw new \PDOException($e->getMessage(), $e->getCode());
        }
    }

    public static function getInstance(): self
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getPdo(): ?\PDO
    {
        return $this->pdo;
    }

    public function getConfiguration()
    {
        return $this->configuration;
    }
     //Check if table exist and if not create it
    public function createTables()
    {
        $tableName = "users";
        $sql = "SHOW TABLES LIKE '$tableName'";
        $result = $this->pdo->query($sql);

        if ($result->rowCount() > 0) {
            
        } else {
            $sql = "CREATE TABLE users (
                id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
                userName char(25) NOT NULL,
                email varchar(255) NOT NULL,
                password varchar(255) NOT NULL,
                token varchar(255) NOT NULL,
                dateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                dateUpdated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP 
            )";
    
            $this->pdo->exec($sql);
        }

        $tableName = "assets";
        $sql = "SHOW TABLES LIKE '$tableName'";
        $result = $this->pdo->query($sql);

        if ($result->rowCount() > 0) {
            
        } else {
            $sql = "CREATE TABLE assets (
                id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
                user_id int(11) NOT NULL,
                imagePath varchar(255) NOT NULL,
                dateCreated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                dateUpdated TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE 
            )";
    
            $this->pdo->exec($sql);
        }
    }
}