<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Db\Database;
use App\Services\UserService;
use App\Services\AssetsService;

class ManagementController extends BaseController
{
    public function index()
    {   
        $loggedIn = false;
        if (isset($_SESSION['email'])) {
            $loggedIn = true;
            $email = $_SESSION['email'];
        }

        $connection = Database::getInstance();
        $pdo = $connection->getPdo();
       
        $userService = new UserService($pdo);
        $users = $userService->getUsers();
        
        $assetsService = new AssetsService($pdo);
        $id = $assetsService->prepareImageUpload($users);
        $assets = $assetsService->getAssets($id);
        
        $this->render('management/index', ['users' => $users, 'loggedIn' => $loggedIn, 'id' => $id]);
    }

    public function asset()
    {
        $connection = Database::getInstance();
        $pdo = $connection->getPdo();
       
        $userService = new UserService($pdo);
        $users = $userService->getUsers();
        
        $assetsService = new AssetsService($pdo);
        $id = $assetsService->prepareImageUpload($users);
        $assets = $assetsService->getAssets($id);
        
        if (isset($_POST['asset_id'])) {
            $asset_id = $_POST['asset_id'];
            $assetsService->removeImage($asset_id);
            $this->redirect('/management');
        }
        
        $registeredUser = $assetsService->uploadImage($id);
        $this->redirect('/management');
    }
}