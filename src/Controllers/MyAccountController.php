<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Services\UserService;
use App\Db\Database;

class MyAccountController extends BaseController
{
    public function index() 
    {   
        $loggedIn = false;
        if (isset($_SESSION['email'])) {
            $loggedIn = true;
            $email = $_SESSION['email'];

            $connection = Database::getInstance();
            $pdo = $connection->getPdo();

            $userService = new UserService($pdo);
            $user = $userService->getUserByEmail($_SESSION['email']);
            
            $this->render('myaccount/index', ['user' => $user, 'loggedIn' => $loggedIn]);
        } else {
            $this->redirect('/login');
        }
    }

    public function user()
    {
        $connection = Database::getInstance();
        $pdo = $connection->getPdo();

        $userService = new UserService($pdo);
    
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (isset($_POST['updatePassword'])) {
                $newPassword = $_POST['newPassword'];
                $email = $_SESSION['email'];
                $user = $userService->updateUser($newPassword, $email);
                
                session_destroy();
                $this->redirect('/login');
            }

            if (isset($_POST['deleteAccount'])) {
                if($_POST['confirmDelete'] === 'DELETE') {
                    $email = $_SESSION['email'];

                    $user = $userService->deleteUser($email);

                    session_destroy();
                    $this->redirect('/');
                }else {
                        $this->redirect('/myaccount');
                }
            }
        }
    }
}