<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Services\AssetsService;
use App\Services\UserService;
use App\Db\Database;

class HomeController extends BaseController 
{
    public function index() 
    {   
        $connection = Database::getInstance();
        $pdo = $connection->getPdo();

        $assetsService = new AssetsService($pdo);
        $result = $assetsService->getImagesCount();

        json_encode(['pictureCount' => $result]);
        
        $userService = new UserService($pdo);
        
        $users = $userService->countRegisteredUsers();
        
        $this->render('home/index', ['users' => $users]);
    }
}