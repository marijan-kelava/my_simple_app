<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Services\UserService;
use App\Db\Database;

class LoginController extends BaseController
{
    public function index()
    {        
        $this->render('login/index', []);
    }

    public function auth()
    {
        $connection = Database::getInstance();
        $pdo = $connection->getPdo();
       
        $userService = new UserService($pdo);

        $this->rememberMe($userService);

        if (isset($_POST['email'])) {
            $email = $_POST['email'];
        }

        if (isset($_POST['password'])) {
            $password = $_POST['password'];
        }
        
        if (!empty($email) && !empty($password)) {
            $registeredUser = $userService->getUserByEmail($email);
        }
        
        if (!empty($registeredUser)) {
            $userService->login($email, $password, $registeredUser);
        }

        if (isset($_SESSION['email'])) {
            $this->redirect('/management');
        }else {
            $this->redirect('/login');
        }
    }

    public function rememberMe($userService)
    {
        if (isset($_COOKIE['rememberMe'])) {
            $token = $_COOKIE['rememberMe'];
            
            $user = $userService->getUserByRememberMeToken($token);
            
            if ($user) {
                $_SESSION['email'] = $user['email'];
                $this->redirect('/management');
            }else {
                $this->redirect('/login');
            }
        }
    }

    public function logout()
    {
        if (isset($_POST['logout'])) {
            $_SESSION = array();

            session_destroy();
            $this->redirect('/');
        } 
    }
}