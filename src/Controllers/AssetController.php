<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Services\AssetsService;
use App\Services\UserService;
use App\Db\Database;

class AssetController extends BaseController 
{
    public function imagesCount() 
    {   
        $connection = Database::getInstance();
        $pdo = $connection->getPdo();
    
        $assetsService = new AssetsService($pdo);
        $result = $assetsService->getImagesCount();
        
        // Return JSON response
        $response = [
            'images_count' => $result['imagesCount']
        ];
    
        header('Content-Type: application/json');
        echo json_encode($response);
    }
}