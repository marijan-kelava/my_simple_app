<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Services\UserService;
use App\Db\Database;

class RegisterController extends BaseController
{
    public function index()
    {
        $this->render('register/index', []);
    }

    public function user()
    {
        $connection = Database::getInstance();
        $pdo = $connection->getPdo();

        $userService = new UserService($pdo);

        $data = $_POST;

        $errors = $userService->validateRegistrationRequest($data);
        
        if($errors){
            $_SESSION['errors'] = $errors;
            $this->redirect('/register');
        }

        $userService->saveNewUser($data);

        session_destroy();
        $this->redirect('/login');
    }
}