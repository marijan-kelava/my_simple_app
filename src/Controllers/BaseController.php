<?php

namespace App\Controllers;

class BaseController 
{
    protected function render($view, $data = []) 
    {
        extract($data);

        include dirname(__FILE__) . "/../Views/$view.php";
    }

    protected function redirect(string $route)
    {
        header("Location: $route");
    }
}
    