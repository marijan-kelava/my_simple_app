<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">
</head>
<body>
    <h1>Login</h1>
    <form action="auth/login" method="post" id="login" novalidate>
        <div>
            <label for="email">email</label>
            <input type="email" id="email" name="email" required>
        </div>
        <div>
            <label for="password">Password</label>
            <input type="password" id="password" name="password" required>
        </div>    
        <button>Login</button>
        <button>
        <a href="/register">Not registered? Register</a>
        </button>
        <div>
            <button>Remember me</button>
        </div>
    </form>
</body>
</html>

