<!DOCTYPE html>
<html>
<head>
    <title>MyAccount</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">
</head>
<body>
    
    <h2>My Account</h2>
    <?php if ($loggedIn): ?>
    <form action="user/myaccount" method="post" id="myaccount" novalidate>
        <div>
            <table style="border: 1px solid black;"> 
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                </tr>
                        <tr>
                            <td><?= $user['userName'] ?></td>
                            <td><?= $user['email'] ?></td>
                        </tr>
            </table>
        <div>
            <label for="newPassword"> New Password</label>
            <input type="newPassword" id="newPassword" name="newPassword" required>
        </div>
        
        <div>
            <label for="password_confirmation">Repeat New Password</label>
            <input type="newPassword" id="password_confirmation" name="password_confirmation" required>
        </div>
        
        <button type="submit" name="updatePassword">Update Password</button>
        <div>
            <input type="text" name="confirmDelete" placeholder="Type 'DELETE' to confirm">
            <button type="submit" name="deleteAccount">Delete Account</button>
        </div>
        <?php endif; ?>
    </form>
    <div>
        <form action="/logout" method="post" id="logout" novalidate>
            <button type="submit" name="logout">Logout</button>
        </form>            
    </div>
</body>
</html>
