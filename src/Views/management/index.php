<!DOCTYPE html>
<html>
<head>
    <title>Management</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">
</head>
    <body>
        <?php if ($loggedIn): ?>
        <h2>Management</h2>
        <div>
                <form action="asset/management" method="post" enctype="multipart/form-data">
                    <label for="image">Select Image:</label>
                        <input type="file" name="image" id="image" accept="image/*">
                    <button type="submit" name="submit">Upload Image</button>
                </form>
            </div>
        <div>
            <table style="border: 1px solid black;"> 
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Image</th>
                    <th>Remove</th>
                </tr>
                        <?php foreach ($users as $user): ?>
                        <tr>
                            <td><?= $user['userName'] ?></td>
                            <td><?= $user['email'] ?></td>
                            <?php if (!empty($user['imagePath'])): ?>
                            <td><img src="<?= $user['imagePath'] ?>" alt="Image"></td>
                            <?php if ($user['id'] == $id): ?>
                            <td>
                            <form action="asset/management" method="post">
                            <input type="hidden" name="asset_id" value="<?= $user['asset_id'] ?>">
                            <button type="submit">Remove</button>
                            <?php endif; ?>
                            </form>
                            </td>
                            <?php endif; ?>
                        </tr>
                        <?php endforeach; ?>
            </table>
                <button>
                <a href="http://localhost:8888/myaccount">My account</a>
                </button>
                <?php endif; ?>   
    </body>
</html>