<!DOCTYPE html>
<html>
<head>
    <title>Home</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/water.css@2/out/water.css">
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
</head>
    <body>
        <h2>Registered Users</h2>
        <div>
            <ul> 
                        <?php foreach ($users as $user): ?>
                            <li><?= $user ?></li>
                        <?php endforeach; ?>
            </ul>
                <button>
                <a href="http://localhost:8888/login">Login</a>
                </button> 
        </div>

        <div id="pictureCount">Click the button to fetch the picture count.</div>
        <button id="fetchPictureCount">Fetch Picture Count</button>

    <script>
        $(document).ready(function () {
            // Function to fetch and display the picture count
            function fetchPictureCount() {
                $.ajax({
                    url: '/assets/count',
                    method: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        $('#pictureCount').text('Number of Pictures: ' + data.images_count);
                    },
                    error: function (error) {
                        console.error('Error:', error);
                        $('#pictureCount').text('Failed to fetch picture count.');
                    }
                });
            }

            // Bind click event to the button
            $('#fetchPictureCount').on('click', function () {
                fetchPictureCount();
            });
        });
    </script>

    </body>
</html>
