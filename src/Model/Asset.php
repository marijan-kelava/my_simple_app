<?php

namespace App\Model;

final class Asset 
{
    public function __construct(
        private int $userId,
        private string $imagePath,
    ) {}

    public function getId() : int
    {
        return $this->id;
    }

    public function getUserId() : int
    {
        return $this->userId;
    }

    public function getImagePath() : string
    {
        return $this->imagePath;
    }
}