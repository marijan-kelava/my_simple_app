<?php

namespace App\Model;

final class User 
{
    public function __construct(
        private string $username,
        private string $email,
        private string $password,
        private string $token
    ) {}

    public function getId() : int
    {
        return $this->id;
    }

    public function getUsername() : string
    {
        return $this->username;
    }

    public function getEmail() : string
    {
        return $this->email;
    }

    public function getPassword() : string
    {
        return $this->password;
    }

    public function getToken() : string
    {
        return $this->token;
    }
}